# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import sleep, time
import logging


def parse_object_tracker_results(object_tracker_string,
                                  patient_id_filter=None, study_uid_filter=None):
    try:
        data = object_tracker_string.split()[1:]
    except IndexError:
        return None
    object_tracker_dict = {}
    for row in data:
        row = row.split(',')
        patient_id = row[0]
        study_uid = row[1]
        series_uid = row[2]
        instances = row[3]
        if patient_id_filter and not patient_id == patient_id_filter:
            logging.debug('Filtering Patient: %s not equal: %s', patient_id, patient_id_filter)
            continue
        if study_uid_filter and not study_uid == study_uid_filter:
            logging.debug('Filtering StudyUID: %s', study_uid, study_uid_filter)
            continue
        if patient_id in object_tracker_dict:
            if study_uid in object_tracker_dict[patient_id]:
                if series_uid in object_tracker_dict[patient_id][study_uid]:
                    object_tracker_dict[patient_id][study_uid][series_uid] = instances
                    logging.debug('Update %s to object_tracker_dict', instances)
                else:
                    object_tracker_dict[patient_id][study_uid][series_uid] = instances
                    logging.debug('Add %s to object_tracker_dict', instances)
            else:
                object_tracker_dict[patient_id][study_uid] = {series_uid: instances}
                logging.debug('Add %s|%s;%s to object_tracker_dict',
                                study_uid, series_uid, instances)
        else:
            logging.debug('Add %s|%s|%s;%s to object_tracker_dict',
                            patient_id, study_uid, series_uid, instances)
            object_tracker_dict[patient_id] = {study_uid: {series_uid: instances}}
    return object_tracker_dict

def parse_idmap_results(idmap_string):
    lines = idmap_string.split('\n')
    _, new = lines[1].split(',')

    if new.startswith('=("'):
        trial_uid = new[3:-2]
        return trial_uid
    return None


def poll_object_tracker_for_studyuid(ctpy_session, pipeline_id,
                                     object_tracker_id, patient_id,
                                     study_uid, delay, timeout):
    old_data = object_tracker_data = {}
    last_update = int(time())
    logging.debug(f'Querying for patient:{patient_id}, study_uid: {study_uid}')
    while old_data == object_tracker_data:
        object_tracker_data = ctpy_session.get_object_tracker_patient_info(pipeline_id,
                                                                   object_tracker_id,
                                                                   '',
                                                                   patient_id,
                                                                   study_uid)
        logging.debug('Checking for new data...')
        if old_data == object_tracker_data:
            logging.debug('No incoming data check timeout')
            if int(time() - last_update) > timeout:
                logging.debug('Timeout reached')
                break
        else:
            last_update = int(time())
            logging.debug('Incoming data...')
            logging.debug(f'Found: {object_tracker_data}')

        
        old_data = object_tracker_data
        sleep(delay)
    return object_tracker_data
